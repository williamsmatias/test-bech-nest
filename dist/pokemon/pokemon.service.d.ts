import { AxiosService } from 'src/common/axios.service';
export declare class PokemonService {
    private readonly axios;
    constructor(axios: AxiosService);
    findAll(limit: number, offset: number): Promise<{
        ok: boolean;
        pokemons: string[];
    }>;
    findByName(name: string): Promise<{
        ok: boolean;
        abilities: import("./interfaces/pokemonDetail.interface").Ability[];
    }>;
}
