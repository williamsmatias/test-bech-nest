"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PokemonService = void 0;
const common_1 = require("@nestjs/common");
const axios_service_1 = require("../common/axios.service");
const POKEMON_API_URL = 'https://pokeapi.co/api/v2/';
let PokemonService = class PokemonService {
    constructor(axios) {
        this.axios = axios;
    }
    async findAll(limit, offset) {
        const url = `${POKEMON_API_URL}/pokemon?limit=${limit}&offset=${offset}`;
        const response = await this.axios.getAxios(url);
        const data = response.results;
        const pokemons = data.map(pokemon => pokemon.name);
        return {
            ok: true,
            pokemons
        };
    }
    async findByName(name) {
        const url = `${POKEMON_API_URL}/pokemon/${name}`;
        const response = await this.axios.getAxios(url);
        const abilities = response.abilities;
        return {
            ok: true,
            abilities
        };
    }
};
PokemonService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [axios_service_1.AxiosService])
], PokemonService);
exports.PokemonService = PokemonService;
//# sourceMappingURL=pokemon.service.js.map