import { PokemonService } from './pokemon.service';
export declare class PokemonController {
    private readonly pokemonService;
    constructor(pokemonService: PokemonService);
    getPokemons(limit: number, offset: number): Promise<{
        ok: boolean;
        pokemons: string[];
    }>;
    getPokemonByName(name: string): Promise<{
        ok: boolean;
        abilities: import("./interfaces/pokemonDetail.interface").Ability[];
    }>;
}
