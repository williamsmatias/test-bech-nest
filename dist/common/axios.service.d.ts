import { HttpService } from '@nestjs/axios';
export declare class AxiosService {
    private readonly httpService;
    constructor(httpService: HttpService);
    getAxios(url: string): Promise<any[]>;
}
