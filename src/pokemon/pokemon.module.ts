import { Module } from '@nestjs/common';
import { CommonModule } from 'src/common/common.module';
import { PokemonController } from './pokemon.controller';
import { PokemonService } from './pokemon.service';

@Module({
  controllers: [PokemonController],
  providers: [PokemonService],
  imports: [CommonModule]
})
export class PokemonModule {}
