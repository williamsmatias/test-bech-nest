
    export interface AbilityDetail {
        name: string;
        url: string;
    }

    export interface Ability {
        ability: AbilityDetail;
        is_hidden: boolean;
        slot: number;
    }

    export interface PokemonDetail {
        abilities: Ability[];
        base_experience: number;
    }



