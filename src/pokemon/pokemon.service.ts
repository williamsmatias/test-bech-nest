import { Injectable } from '@nestjs/common';
import { AxiosService } from 'src/common/axios.service';
import { PokemonDetail } from './interfaces/pokemonDetail.interface';
import { PokemonList } from './interfaces/pokemonsList.interface';

const POKEMON_API_URL = 'https://pokeapi.co/api/v2/';

@Injectable()
export class PokemonService {

    constructor( private readonly axios: AxiosService ) {}
    
    async findAll( limit: number, offset: number) {
        const url = `${POKEMON_API_URL}/pokemon?limit=${limit}&offset=${offset}`;
        const response = <PokemonList> <any> await this.axios.getAxios(url);
        const data = response.results;
        const pokemons = data.map(pokemon => pokemon.name);
        return {
            ok: true,
            pokemons
        };
    }

    async findByName( name: string ) {
        const url = `${POKEMON_API_URL}/pokemon/${name}`;
        const response = <PokemonDetail><any> await this.axios.getAxios(url);
        const abilities = response.abilities; 
        return {
            ok: true,
            abilities
        };
    }

    
}
