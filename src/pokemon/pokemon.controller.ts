import { Controller, Get, Param, ParseIntPipe, Query } from '@nestjs/common';
import { PokemonService } from './pokemon.service';

@Controller('pokemon')
export class PokemonController {

    constructor( private readonly pokemonService: PokemonService ) {}

    @Get('')
    getPokemons(
        @Query('limit', ParseIntPipe) limit: number,
        @Query('offset', ParseIntPipe) offset: number) {
        return this.pokemonService.findAll(limit, offset);
    }

    @Get(':name')
    getPokemonByName(@Param('name') name: string) {
        return this.pokemonService.findByName(name);
    }
}
