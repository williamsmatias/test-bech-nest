import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { AxiosError } from 'axios';
import { catchError, firstValueFrom } from 'rxjs';

@Injectable()
export class AxiosService {
    constructor(private readonly httpService: HttpService) {}

    async getAxios( url: string ): Promise<any[]> {
        const { data } = await firstValueFrom(
          this.httpService.get<any[]>( url ).pipe(
            catchError((error: AxiosError) => {
              console.log(error.response.data);
              throw 'Error en axios request';
            }),
          ),
        );

        return data;
    }
}
